using System;

public class Vehicle
{
    public Vehicle()
    {
        Console.WriteLine("In Vehicle constructor");
    }

    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    } 
    private int _weight = 0;
    public int Weight 
    {
        get 
        {
            return _weight;
        }
        set
        {
            if (value > 0)
                _weight = value;
        }
    }
    private string _country;
    public string Country
    {
        get 
        {
            return _country;
        }
        set 
        {
            _country = value;
        }
    }
    private int _year;
    public int Year
    {
        get 
        {
            return _year;
        }
        set 
        {
            _year = value;
        }
    }
    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set 
        {
            if (value >= 0)
                _value = value;
        }
    }

    public virtual void ShowInfo()
    {
        Console.WriteLine("The vehicle is not chosen");
    }
}

public class Vertibird: Vehicle
{
    private static string class_name = "Vertibird";
    private static string type = "Aerial";

    private static Vertibird instance;

    private Vertibird()
    {
        Console.WriteLine("In Vertibird private constructor");
    } 

    public static string GetClassName()
    {
        return class_name;
    }

    public static string GetClassType()
    {
        return type;
    }

    public static Vertibird getInstance()
    {
        if (instance == null)
            instance = new Vertibird();
        return instance;
    }
}

public class Car: Vehicle
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int doorsAmount;
    public Car(string name, int weight, string country, int year, int doorsAm, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        doorsAmount = doorsAm;
        Value = value;
        Console.WriteLine("In car constructor");
    }
    public Car(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Car()
    {
        class_name = "Car";
        Console.WriteLine("In car static constructor");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Doors amount: " + doorsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Car.class_name);
    }
}

public class Bicycle: Vehicle
{
    private static string class_name = "Bicycle";
    public const string type = "Terrestrial";

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Bicycle.class_name);
    }
}

public class Airplane: Vehicle
{
    private static string class_name;
    public const string type = "Aerial";
    public readonly int maxFlightHeight;
    public Airplane(string name, int weight, string country, int year, int maxFlightHeight_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        maxFlightHeight = maxFlightHeight_;
        Value = value;
    }
    public Airplane(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Airplane()
    {
        class_name = "Airplane";
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Max flight height: " + maxFlightHeight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Airplane.class_name);
    }   
}

public class Boat: Vehicle
{
    private static string class_name;
    public const string type = "Aquatic";
    public readonly int deadweight;
    private Boat() {}
    public Boat(string name, int weight, string country, int year, int dead_weight, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        deadweight = dead_weight;
        Value = value;
    }
    public Boat(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Boat()
    {
        class_name = "Boat";
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Deadweight: " + deadweight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Boat.class_name);
    }   
}

public class Motorbike: Vehicle
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int wheelsAmount;
    private Motorbike() {}
    public Motorbike(string name, int weight, string country, int year, int wheelsAmount_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        wheelsAmount = wheelsAmount_;
        Value = value;
    }
    public Motorbike(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Motorbike()
    {
        class_name = "Motorbike";
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Wheels amount: " + wheelsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Motorbike.class_name);
    }     
}

class Program
{
    static void Main()
    {
        var vehicle = new Vehicle();
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Car("Fiat", 1490, "France", 2010, 4, 9999);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Bicycle();
        vehicle.ShowInfo();     
        Console.WriteLine("---------------------------------------------");
        vehicle = new Airplane("Boeing", 10000, "USA", 2004, 1500, 5000000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Boat("Boat", 10000, "China", 2005, 20000, 4000000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Motorbike("Ducatti", 330, "Italy", 2014, 2, 15000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = Vertibird.getInstance();
        Console.WriteLine(Vertibird.GetClassName());
        Console.WriteLine(Vertibird.GetClassType());
    }
}