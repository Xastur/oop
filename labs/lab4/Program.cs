﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Threading.Tasks;

interface IColor
{
    void WriteInterfaceName();
    void SetInteriorColor(string color);
    void WriteInteriorColor();
    void SetBodyColor(string color);
    void WriteBodyColor();
    void SetWheelsColor(string color);
    void WriteWheelsColor();
}

interface IPassportData
{
    void WriteInterfaceName();
    void WriteName();
    void WriteWeight();
    void WriteCountry();
    void WriteYear();
    void WriteValue();
}

interface ICarValue
{
    void WriteCarValue();
}

public interface IComparable
{
    int CompareTo(object o);
}

interface IComparer
{
    int Compare(object o1, object o2);
}

[Serializable]
abstract public class Vehicle : IComparable
{
    private bool disposed = false;

    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    private int _weight = 0;
    public int Weight
    {
        get
        {
            return _weight;
        }
        set
        {
            if (value > 0)
                _weight = value;
        }
    }
    private string _country;
    public string Country
    {
        get
        {
            return _country;
        }
        set
        {
            _country = value;
        }
    }
    private int _year;
    public int Year
    {
        get
        {
            return _year;
        }
        set
        {
            _year = value;
        }
    }
    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            if (value >= 0)
                _value = value;
        }
    }

    // public Params p = new Params();
    public List<string> repairHistory = new List<string>();

    public IEnumerator GetEnumerator()
    {
        return repairHistory.GetEnumerator();
    }

    int IComparable.CompareTo(object o)
    {
        Vehicle veh = o as Vehicle;
        if (veh != null)
            return this.Name.CompareTo(veh.Name);
        else
            throw new Exception("Невозможно сравнить два объекта");
    }

    public abstract void SetCondition(string condition);
    public abstract string GetCondition();

    public virtual void ShowInfo()
    {
        Console.WriteLine("The vehicle is not chosen");
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposed)
        {
            if (disposing)
            {
                Console.WriteLine("In Dispose");
            }
            disposed = true;
        }
    }

    ~Vehicle()
    {
        Console.WriteLine("In Destructor");
        Dispose(false);
    }
}

public class VList
{
    public List<Vehicle> vehiclesList = new List<Vehicle>();

    public Vehicle this[string name]
    {
        get
        {
            return vehiclesList.Find(x => x.Name == name);
        }
        set
        {
            Vehicle v = vehiclesList.Find(x => x.Name == name);
            if (v != null)
            {
                vehiclesList.Remove(v);
                vehiclesList.Add(value);
            }
        }
    }
}

public static class VListExtension
{
    public static int VehicleCount(this List<Vehicle> vehicles, string name) //By name
    {
        List<Vehicle> list = vehicles.FindAll(x => x.Name == name);
        return list.Count;
    }
}

class VehiclesComparer : IComparer<Vehicle>
{
    public int Compare(Vehicle v1, Vehicle v2)
    {
        if (v1.Name.Length > v2.Name.Length)
            return 1;
        else if (v1.Name.Length < v2.Name.Length)
            return -1;
        else
            return 0;
    }
}

public class Car : Vehicle, IPassportData, IColor
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int doorsAmount;
    private string bodyColor;
    private string interiorColor;
    private string wheelsColor;
    private string _condition;

    public Car() { }

    public Car(string name, int weight, string country, int year, int doorsAm, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        doorsAmount = doorsAm;
        Value = value;
        Console.WriteLine("In car constructor");
    }
    public Car(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Car()
    {
        class_name = "Car";
        Console.WriteLine("In car static constructor");
    }

    ~Car()
    {
        Console.WriteLine("In Car destructor");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public void WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IColor");
    }

    public void SetInteriorColor(string color)
    {
        interiorColor = color;
    }

    public void SetBodyColor(string color)
    {
        bodyColor = color;
    }

    public void SetWheelsColor(string color)
    {
        wheelsColor = color;
    }

    public void WriteInteriorColor()
    {
        Console.WriteLine("Interior color: " + interiorColor);
    }

    public void WriteBodyColor()
    {
        Console.WriteLine("Body color: " + bodyColor);
    }

    public void WriteWheelsColor()
    {
        Console.WriteLine("Wheels color: " + wheelsColor);
    }

    public override void SetCondition(string condition)
    {
        this._condition = condition;
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return this._condition;
        throw new Exception("No Condition");
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Car(Name, Weight, Country, Year, doorsAmount, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Doors amount: " + doorsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Car.class_name);
        var link2 = (IColor)vehicle;
        link2.SetBodyColor("Red");
        link2.SetInteriorColor("Black");
        link2.SetWheelsColor("White");
        link2.WriteInterfaceName();
        link2.WriteBodyColor();
        link2.WriteInteriorColor();
        link2.WriteWheelsColor();
    }
}

public class Airplane : IPassportData
{
    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    private int _weight = 0;
    public int Weight
    {
        get
        {
            return _weight;
        }
        set
        {
            if (value > 0)
                _weight = value;
        }
    }
    private string _country;
    public string Country
    {
        get
        {
            return _country;
        }
        set
        {
            _country = value;
        }
    }
    private int _year;
    public int Year
    {
        get
        {
            return _year;
        }
        set
        {
            _year = value;
        }
    }
    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            if (value >= 0)
                _value = value;
        }
    }

    private static string class_name;
    public const string type = "Aerial";
    public readonly int maxFlightHeight;
    private string _condition;
    public Airplane(string name, int weight, string country, int year, int maxFlightHeight_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        maxFlightHeight = maxFlightHeight_;
        Value = value;
    }
    public Airplane(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Airplane()
    {
        class_name = "Airplane";
    }

    public void SetCondition(string condition)
    {
        _condition = condition;
    }

    public string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public void ShowInfo()
    {
        Airplane airplane = new Airplane(Name, Weight, Country, Year, maxFlightHeight, Value);
        var link1 = (IPassportData)airplane;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Max flight height: " + maxFlightHeight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Airplane.class_name);
    }

    public static Airplane currentInstance = null;
    private bool hasFinalized = false;

    ~Airplane()
    {
        Console.WriteLine("In Airplane destrustor");
        if (hasFinalized == false)
        {
            Console.WriteLine("First finalization");

            // Put this object back into a root by creating
            // a reference to it.
            Airplane.currentInstance = this;

            // Indicate that this instance has finalized once.
            hasFinalized = true;

            // Place a reference to this object back in the
            // finalization queue.
            GC.ReRegisterForFinalize(this);
        }
        else
        {
            Console.WriteLine("Second finalization");
        }
    }
}

public class VehicleCondition
{
    private VehicleCondition() { }
    public static string GetGreatCondition()
    {
        return "Great";
    }
    public static string GetGoodCondition()
    {
        return "Good";
    }
    public static string GetNormalCondition()
    {
        return "Normal";
    }
    public static string GetWrackedCondition()
    {
        return "Wracked";
    }
}

public class MyException : Exception
{
    public MyException(string message)
        : base(message)
    {
        if (message == VehicleCondition.GetWrackedCondition())
        {
            Console.WriteLine("In MyException class, GetWrackedCondition()");
        }
    }
}

class Handler
{
    public void Message(string message, Vehicle vehicle)
    {
        if (message == "Cheap")
        {
            try
            {
                if (vehicle.GetCondition() == VehicleCondition.GetWrackedCondition())
                {
                    Console.WriteLine("Repairing...");
                    vehicle.SetCondition(VehicleCondition.GetGreatCondition());
                    Console.WriteLine("Repairing done!");
                }
                else
                {
                    Console.WriteLine("Vehicle is in good condition. No repair needed");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
        else
        {
            Console.WriteLine("The vehicle is too expensive. Can not reapir if needed");
        }
        Console.WriteLine(message);
    }
}

public class CarValue
{
    //Func 
    //----------------------------------------

    public void getCarValue(Vehicle car)
    {
        Func<Vehicle, string> func = carValueCalc;
        getString(car, func);
    }

    void getString(Vehicle car, Func<Vehicle, string> func)
    {
        Console.WriteLine(func(car));
    }

    static string carValueCalc(Vehicle car)
    {
        if (car.Value < 10000)
        {
            return "Cheap";
        }
        else if (car.Value < 30000)
        {
            return "Comfort";
        }
        else if (car.Value < 100000)
        {
            return "Business";
        }
        else
        {
            return "Luxurious";
        }
    }
}

class ListEnumerator : IEnumerator<string>
{
    string[] list;
    int position = -1;
    public ListEnumerator(string[] list)
    {
        this.list = list;
    }

    public string Current
    {
        get
        {
            if (position == -1 || position >= list.Length)
                throw new InvalidOperationException();
            return list[position];
        }
    }

    object IEnumerator.Current => throw new NotImplementedException();

    public bool MoveNext()
    {
        if (position < list.Length - 1)
        {
            position++;
            return true;
        }
        else
            return false;
    }

    public void Reset()
    {
        position = -1;
    }
    public void Dispose() { }
}

public class Cache
{
    // Dictionary to contain the cache.
    static Dictionary<int, WeakReference> _cache;

    // Track the number of times an object is regenerated.
    int regenCount = 0;

    public Cache(int count)
    {
        _cache = new Dictionary<int, WeakReference>();

        // Add objects with a short weak reference to the cache.
        for (int i = 0; i < count; i++)
        {
            _cache.Add(i, new WeakReference(new Data(i), false));
        }
    }

    // Number of items in the cache.
    public int Count
    {
        get { return _cache.Count; }
    }

    // Number of times an object needs to be regenerated.
    public int RegenerationCount
    {
        get { return regenCount; }
    }

    // Retrieve a data object from the cache.
    public Data this[int index]
    {
        get
        {
            Data d = _cache[index].Target as Data;
            if (d == null)
            {
                // If the object was reclaimed, generate a new one.
                Console.WriteLine("Regenerate object at {0}: Yes", index);
                d = new Data(index);
                _cache[index].Target = d;
                regenCount++;
            }
            else
            {
                // Object was obtained with the weak reference.
                Console.WriteLine("Regenerate object at {0}: No", index);
            }

            return d;
        }
    }
}

// This class creates byte arrays to simulate data.
public class Data
{
    private byte[] _data;
    private string _name;

    public Data(int size)
    {
        _data = new byte[size * 1024];
        _name = size.ToString();
    }

    // Simple property.
    public string Name
    {
        get { return _name; }
    }
}

class Program
{
    static void Main()
    {
        Vehicle vehicle;
        vehicle = new Car("Fiat", 1490, "France", 2010, 4, 9999);

        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        Console.Write("Total memory: " + GC.GetTotalMemory(false).ToString() + "\n");
        GC.Collect();
        vehicle = null;
        Airplane airplane = new Airplane("Boeing", 10000, "USA", 2004, 1500, 5000000);
        Console.WriteLine("Generation: " + GC.GetGeneration(airplane));
        airplane.ShowInfo();
        GC.Collect();
        Console.WriteLine("Generation: " + GC.GetGeneration(airplane));
        Console.WriteLine("---------------------------------------------");
        Airplane a = null;

        // Create and release a large number of objects
        // that require finalization.
        for (int j = 0; j < 10; j++)
        {
            a = new Airplane("Boeing", 10000, "USA", 2004, 1500, 5000000);
        }

        //Release the last object created in the loop.
        a = null;

        //Force garbage collection.
        GC.Collect();

        // Wait for all finalizers to complete before continuing.
        // Without this call to GC.WaitForPendingFinalizers, 
        // the worker loop below might execute at the same time 
        // as the finalizers.
        // With this call, the worker loop executes only after
        // all finalizers have been called.
        GC.WaitForPendingFinalizers();

        // Worker loop to perform post-finalization code.
        for (int i = 0; i < 1; i++)
        {
            Console.WriteLine("Doing some post-finalize work");
        }

        // Create the cache.
        int cacheSize = 50;
        Random r = new Random();
        Cache c = new Cache(cacheSize);

        string DataName = "";
        GC.Collect(0);

        // Randomly access objects in the cache.
        for (int i = 0; i < c.Count; i++)
        {
            int index = r.Next(c.Count);

            // Access the object by getting a property value.
            DataName = c[index].Name;
        }
        // Show results.
        double regenPercent = c.RegenerationCount / (double)c.Count;
        Console.WriteLine("Cache size: {0}, Regenerated: {1:P2}%", c.Count, regenPercent);
    }
}