﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Threading.Tasks;

interface IColor
{
    void WriteInterfaceName();
    void SetInteriorColor(string color);
    void WriteInteriorColor();
    void SetBodyColor(string color);
    void WriteBodyColor();
    void SetWheelsColor(string color);
    void WriteWheelsColor();
}

interface IPassportData
{
    void WriteInterfaceName();
    void WriteName();
    void WriteWeight();
    void WriteCountry();
    void WriteYear();
    void WriteValue();
}

interface ICarValue
{
    void WriteCarValue();
}

public interface IComparable
{
    int CompareTo(object o);
}

interface IComparer
{
    int Compare(object o1, object o2);
}

[Serializable]
abstract public class Vehicle: IComparable
{
    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    private int _weight = 0;
    public int Weight
    {
        get
        {
            return _weight;
        }
        set
        {
            if (value > 0)
                _weight = value;
        }
    }
    private string _country;
    public string Country
    {
        get
        {
            return _country;
        }
        set
        {
            _country = value;
        }
    }
    private int _year;
    public int Year
    {
        get
        {
            return _year;
        }
        set
        {
            _year = value;
        }
    }
    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            if (value >= 0)
                _value = value;
        }
    }

    // public Params p = new Params();
    public Passport<string, Params> passport = new Passport<string, Params>();
    public List<string> repairHistory = new List<string>();

    public IEnumerator GetEnumerator()
    {
        return repairHistory.GetEnumerator();
    }

    int IComparable.CompareTo(object o)
    {
        Vehicle veh = o as Vehicle;
        if (veh != null)
            return this.Name.CompareTo(veh.Name);
        else
            throw new Exception("Невозможно сравнить два объекта");
    }

    public abstract void SetCondition(string condition);
    public abstract string GetCondition();

    public virtual void ShowInfo()
    {
        Console.WriteLine("The vehicle is not chosen");
    }
}

[Serializable]
public class VList
{
    public List<Vehicle> vehiclesList = new List<Vehicle>();

    public Vehicle this[string name] 
    {
        get
		{
            return vehiclesList.Find(x => x.Name == name);
		}
		set
		{
            Vehicle v = vehiclesList.Find(x => x.Name == name);
            if (v != null)
            {
                vehiclesList.Remove(v);
                vehiclesList.Add(value);
            }
		}
    }
}

public static class VListExtension
{
    public static int VehicleCount(this List<Vehicle> vehicles, string name) //By name
    {
        List<Vehicle> list = vehicles.FindAll(x => x.Name == name);
        return list.Count;
    }
}

class VehiclesComparer : IComparer<Vehicle>
{
    public int Compare(Vehicle v1, Vehicle v2)
    {
        if (v1.Name.Length > v2.Name.Length)
            return 1;
        else if (v1.Name.Length < v2.Name.Length)
            return -1;
        else
            return 0;
    }
}

public class Notifier
{
    public void Message()
    {
        Console.WriteLine("HELLO");
    }
}

[Serializable]
public class Car : Vehicle, IPassportData, IColor
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int doorsAmount;
    private string bodyColor;
    private string interiorColor;
    private string wheelsColor;
    private string _condition;

    public Car() {}

    public Car(string name, int weight, string country, int year, int doorsAm, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        doorsAmount = doorsAm;
        Value = value;
        Console.WriteLine("In car constructor");
    }
    public Car(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Car()
    {
        class_name = "Car";
        Console.WriteLine("In car static constructor");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public void WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IColor");
    }

    public void SetInteriorColor(string color)
    {
        interiorColor = color;
    }

    public void SetBodyColor(string color)
    {
        bodyColor = color;
    }

    public void SetWheelsColor(string color)
    {
        wheelsColor = color;
    }

    public void WriteInteriorColor()
    {
        Console.WriteLine("Interior color: " + interiorColor);
    }

    public void WriteBodyColor()
    {
        Console.WriteLine("Body color: " + bodyColor);
    }

    public void WriteWheelsColor()
    {
        Console.WriteLine("Wheels color: " + wheelsColor);
    }

    public override void SetCondition(string condition)
    {
        this._condition = condition;    
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return this._condition;
        throw new Exception("No Condition");
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Car(Name, Weight, Country, Year, doorsAmount, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Doors amount: " + doorsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Car.class_name);
        var link2 = (IColor)vehicle;
        link2.SetBodyColor("Red");
        link2.SetInteriorColor("Black");
        link2.SetWheelsColor("White");
        link2.WriteInterfaceName();
        link2.WriteBodyColor();
        link2.WriteInteriorColor();
        link2.WriteWheelsColor();
        passport.ShowInfo();
    }
}

public class Bicycle : Vehicle
{
    private static string class_name = "Bicycle";
    public const string type = "Terrestrial";

    private string _condition;

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No Condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Bicycle.class_name);
    }
}

public class Airplane : Vehicle, IPassportData
{
    private static string class_name;
    public const string type = "Aerial";
    public readonly int maxFlightHeight;
    private string _condition;
    private Airplane() { }
    public Airplane(string name, int weight, string country, int year, int maxFlightHeight_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        maxFlightHeight = maxFlightHeight_;
        Value = value;
    }
    public Airplane(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Airplane()
    {
        class_name = "Airplane";
    }

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Airplane(Name, Weight, Country, Year, maxFlightHeight, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Max flight height: " + maxFlightHeight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Airplane.class_name);
    }
}

public class Boat : Vehicle
{
    private static string class_name;
    public const string type = "Aquatic";
    public readonly int deadweight;
    private string _condition;
    private Boat() { }
    public Boat(string name, int weight, string country, int year, int dead_weight, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        deadweight = dead_weight;
        Value = value;
    }
    public Boat(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Boat()
    {
        class_name = "Boat";
    }

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }
    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Deadweight: " + deadweight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Boat.class_name);
    }
}

public class Motorbike : Vehicle, IPassportData
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int wheelsAmount;
    private string _condition;
    private Motorbike() { }
    public Motorbike(string name, int weight, string country, int year, int wheelsAmount_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        wheelsAmount = wheelsAmount_;
        Value = value;
    }
    public Motorbike(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Motorbike()
    {
        class_name = "Motorbike";
    }

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }
    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Motorbike(Name, Weight, Country, Year, wheelsAmount, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Wheels amount: " + wheelsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Motorbike.class_name);
    }
}

public class Vertibird : Vehicle
{
    private static string class_name = "Vertibird";
    private static string type = "Aerial";

    private static Vertibird instance;

    private Vertibird()
    {
        Console.WriteLine("In Vertibird private constructor");
    }

    public override void SetCondition(string condition) { }
    public override string GetCondition() { return "static"; }

    public static string GetClassName()
    {
        return class_name;
    }

    public static string GetClassType()
    {
        return type;
    }

    public static Vertibird getInstance()
    {
        if (instance == null)
            instance = new Vertibird();
        return instance;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Info: Vertibird");
    }
}

public class VehicleCondition
{
    private VehicleCondition() { }
    public static string GetGreatCondition()
    {
        return "Great";
    }
    public static string GetGoodCondition()
    {
        return "Good";
    }
    public static string GetNormalCondition()
    {
        return "Normal";
    }
    public static string GetWrackedCondition()
    {
        return "Wracked";
    }
}

public class MyException : Exception
{
    public MyException(string message)
        : base(message)
    {
        if (message == VehicleCondition.GetWrackedCondition())
        {
            Console.WriteLine("In MyException class, GetWrackedCondition()");
        }
    }
}

class Handler
{
    public void Message(string message, Vehicle vehicle)
    {
        if (message == "Cheap")
        {
            try
            {
                if (vehicle.GetCondition() == VehicleCondition.GetWrackedCondition())
                {
                    Console.WriteLine("Repairing...");
                    vehicle.SetCondition(VehicleCondition.GetGreatCondition());
                    Console.WriteLine("Repairing done!");
                }
                else 
                {
                    Console.WriteLine("Vehicle is in good condition. No repair needed");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
        else 
        {
            Console.WriteLine("The vehicle is too expensive. Can not reapir if needed");
        }
        Console.WriteLine(message);
    }
}

public class CarValue
{
    //anonim

//     delegate void MethodContainer(string message); 

//     public void getCarValue(Vehicle car)
//     {
//         MethodContainer Notify = delegate(string mes) 
//         {
//             Console.WriteLine(mes);
//         };
//         if (car.Value < 10000)
//         {
//             Notify("Cheap");
//         }
//         else if (car.Value < 30000)
//         {
//             Notify("Comfort");
//         }
//         else if (car.Value < 100000)
//         {
//             Notify("Business");
//         }
//         else
//         {
//             Notify("Luxurious");
//         }
//     }

    //lambda
    //-----------------------------------------------------

//     delegate void MethodContainer(string message);

//     public void getCarValue(Vehicle car) 
//     {
//         MethodContainer Notify = (mes) => Console.WriteLine(mes);
        
//         if (car.Value < 10000)
//         {
//             Notify("Cheap");
//         }
//         else if (car.Value < 30000)
//         {
//             Notify("Comfort");
//         }
//         else if (car.Value < 100000)
//         {
//             Notify("Business");
//         }
//         else
//         {
//             Notify("Luxurious");
//         }
//     }

    // Action 
    // ----------------------------------------

//     public void getCarValue(Vehicle car) 
//     {
//         Action<Vehicle> act = carValueCalc;
//         act(car);
//     }

//     static void carValueCalc(Vehicle car)
//     {
//         if (car.Value < 10000)
//         {
//             Console.WriteLine("Cheap");
//         }
//         else if (car.Value < 30000)
//         {
//             Console.WriteLine("Comfort");
//         }
//         else if (car.Value < 100000)
//         {
//             Console.WriteLine("Business");
//         }
//         else
//         {
//             Console.WriteLine("Luxurious");
//         }   
//     }

    //Func 
    //----------------------------------------

    public void getCarValue(Vehicle car) 
    {
        Func<Vehicle, string> func = carValueCalc;
        getString(car, func);
    }

    void getString(Vehicle car, Func<Vehicle, string> func)
    {
        Console.WriteLine(func(car));
    }
    
    static string carValueCalc(Vehicle car)
    {
        if (car.Value < 10000)
        {
            return "Cheap";
        }
        else if (car.Value < 30000)
        {
            return "Comfort";
        }
        else if (car.Value < 100000)
        {
            return "Business";
        }
        else
        {
            return "Luxurious";
        }   
    }
}

class ListEnumerator : IEnumerator<string>
{
    string[] list;
    int position = -1;
    public ListEnumerator(string[] list)
    {
        this.list = list;
    }
     
    public string Current
    {
        get
        {
            if (position == -1 || position >= list.Length)
                throw new InvalidOperationException();
            return list[position];
        }
    }

    object IEnumerator.Current => throw new NotImplementedException();
     
    public bool MoveNext()
    {
        if(position < list.Length - 1)
        {
            position++;
            return true;
        }
        else
            return false;
    }

    public void Reset()
    {
        position = -1;
    }
    public void Dispose() { }
}

public class Params 
{
    public int MaxSpeed = 200;
}

// [Serializable]
public class Passport<T, P> where P: Params
{
    public T Index { get; set; }
    public T ManufacturerCode { get; set; }
    public P Param;

    public P MaxSpeed 
    {
        get
        { 
            return Param;
        }
        set
        {
            Param = value;
        }
    }

    public void ShowInfo()
    {
        Console.WriteLine("Index: " + Index);
        Console.WriteLine("Manufacturer code: " + ManufacturerCode);
    }
}

class Program
{                  
    static async Task Main()
    {
        VList vList = new VList();

        Vehicle v1;
        v1 = new Car("Ferrari", 1400, "Italy", 2016, 2, 500000);

        BinaryFormatter formatter = new BinaryFormatter();

        // using (FileStream fs = new FileStream("ferrari.dat", FileMode.OpenOrCreate))
        // {
        //     formatter.Serialize(fs, v1);

        //     Console.WriteLine("Объект сериализован");
        // }

        // using (FileStream fs = new FileStream("ferrari.dat", FileMode.OpenOrCreate))
        // {
        //     Vehicle v12 = (Vehicle)formatter.Deserialize(fs);

        //     Console.WriteLine("Объект десериализован");
        //     Console.WriteLine($"Name: {v12.Name} --- Value: {v12.Value} --- {v12}");
        // }

        Vehicle vv2; 
        vv2 = new Car("BMW", 1650, "Germany", 2012, 4, 45000);
        vv2.passport.Param = new Params();
        vv2.passport.ManufacturerCode = "X90";
        vv2.passport.Index = "23912";
        Console.WriteLine(vv2.passport.MaxSpeed);
        vv2.ShowInfo();

        vList.vehiclesList.Add(vv2);
        vList.vehiclesList.Add(v1);

        // using (FileStream fs = new FileStream("list.json", FileMode.OpenOrCreate))
        // {
        //     await JsonSerializer.SerializeAsync<List<Vehicle>>(fs, vList.vehiclesList);
        //     Console.WriteLine("Data has been saved to file");
        // }

        // чтение данных
        // using (FileStream fs = new FileStream("list.json", FileMode.OpenOrCreate))
        // {
        //     List<Car> restoredList = await JsonSerializer.DeserializeAsync<List<Car>>(fs);
        //     Console.WriteLine($"List: {restoredList} --- {restoredList[0].Name}");
        // }

        Vehicle vehicle;

        vehicle = new Car("Fiat", 1490, "France", 2010, 4, 9999);

        Console.WriteLine("-------------------------------");
        Console.WriteLine(vList.vehiclesList.VehicleCount("Fiat"));
        vList.vehiclesList.Add(vehicle);
        Console.WriteLine(vList.vehiclesList.VehicleCount("Fiat"));
        Console.WriteLine("-------------------------------");

        Vehicle[] vehList = new Vehicle[] {v1, vv2, vehicle};

        // using (FileStream fs = new FileStream("vehicles.dat", FileMode.OpenOrCreate))
        // {
        //     formatter.Serialize(fs, vehList); 
        // }

        // using (FileStream fs = new FileStream("vehicles.dat", FileMode.OpenOrCreate))
        // {
        //     Vehicle[] deserilizeVehicles = (Vehicle[])formatter.Deserialize(fs);

        //     foreach (Vehicle v in deserilizeVehicles)
        //     {
        //         Console.WriteLine($"Name: {v.Name} --- Value: {v.Value}");
        //     }
        // }

        foreach(Vehicle v in vehList)
            Console.WriteLine(v.Name);

        Array.Sort(vehList, new VehiclesComparer());

        foreach(Vehicle v in vehList)
            Console.WriteLine(v.Name);

        vehicle.SetCondition(VehicleCondition.GetGoodCondition());
        Handler handler = new Handler();
        CarValue carValue = new CarValue();
        carValue.getCarValue(vehicle);
        vehicle.ShowInfo();
        vehicle.repairHistory.Add("1");
        vehicle.repairHistory.Add("2");
        vehicle.repairHistory.Add("3");
        foreach (string x in vehicle)
            Console.WriteLine(x);
        vehicle.repairHistory.RemoveAt(1);
        foreach (string x in vehicle.repairHistory)
            Console.WriteLine(x);
        Console.WriteLine(vehicle.repairHistory.Find(x => x == "3"));
        List<Vehicle> vehicleList = new List<Vehicle>();
        vehicleList.Add(vehicle);
        Vehicle v2 = vehicleList.Find(x => x.Name == "Fiat");
        Console.WriteLine(v2.Name);
        Console.WriteLine("---------------------------------------------");
        vehicle = new Bicycle();
        try 
        {
            Console.WriteLine(vehicle.GetCondition());
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Airplane("Boeing", 10000, "USA", 2004, 1500, 5000000);
        vList["Fiat"] = vehicle;
        Console.WriteLine("---------------------------");
        foreach (Vehicle x in vList.vehiclesList)
            Console.WriteLine(x);        
        Console.WriteLine("---------------------------");
        vehicle.ShowInfo();
        // vehicleList.Add(vehicle);
        Console.WriteLine(vehicleList.Find(x => x.Name == "Boeing"));
        Console.WriteLine("---------------------------------------------");
        vehicle = new Boat("Boat", 10000, "China", 2005, 20000, 4000000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Motorbike("Ducatti", 330, "Italy", 2014, 2, 15000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = Vertibird.getInstance();
        vehicle.ShowInfo();
    }
}