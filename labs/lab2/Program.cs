﻿using System;

interface IColor
{
    void WriteInterfaceName();
    void SetInteriorColor(string color);
    void WriteInteriorColor();
    void SetBodyColor(string color);
    void WriteBodyColor();
    void SetWheelsColor(string color);
    void WriteWheelsColor();
}

interface IPassportData
{
    void WriteInterfaceName();
    void WriteName();
    void WriteWeight();
    void WriteCountry();
    void WriteYear();
    void WriteValue();
}

abstract public class Vehicle
{
    public string _condition;
    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    private int _weight = 0;
    public int Weight
    {
        get
        {
            return _weight;
        }
        set
        {
            if (value > 0)
                _weight = value;
        }
    }
    private string _country;
    public string Country
    {
        get
        {
            return _country;
        }
        set
        {
            _country = value;
        }
    }
    private int _year;
    public int Year
    {
        get
        {
            return _year;
        }
        set
        {
            _year = value;
        }
    }
    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            if (value >= 0)
                _value = value;
        }
    }

    public abstract void SetCondition(string condition);
    public abstract string GetCondition();

    public virtual void ShowInfo()
    {
        Console.WriteLine("The vehicle is not chosen");
    }
}

public class Car : Vehicle, IPassportData, IColor
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int doorsAmount;
    private string bodyColor;
    private string interiorColor;
    private string wheelsColor;
    // private string _condition;

    public Car(string name, int weight, string country, int year, int doorsAm, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        doorsAmount = doorsAm;
        Value = value;
        Console.WriteLine("In car constructor");
    }
    public Car(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Car()
    {
        class_name = "Car";
        Console.WriteLine("In car static constructor");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public void WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IColor");
    }

    public void SetInteriorColor(string color)
    {
        interiorColor = color;
    }

    public void SetBodyColor(string color)
    {
        bodyColor = color;
    }

    public void SetWheelsColor(string color)
    {
        wheelsColor = color;
    }

    public void WriteInteriorColor()
    {
        Console.WriteLine("Interior color: " + interiorColor);
    }

    public void WriteBodyColor()
    {
        Console.WriteLine("Body color: " + bodyColor);
    }

    public void WriteWheelsColor()
    {
        Console.WriteLine("Wheels color: " + wheelsColor);
    }

    public override void SetCondition(string condition)
    {
        // if (condition == VehicleCondition.GetWrackedCondition())
        // {
        //     Console.WriteLine("Throw an error");
        //     throw new MyException(VehicleCondition.GetWrackedCondition());
        // }
        // else
        // {
            this._condition = condition;
        // }
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return this._condition;
        throw new Exception("No Condition");
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Car(Name, Weight, Country, Year, doorsAmount, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Doors amount: " + doorsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Car.class_name);
        var link2 = (IColor)vehicle;
        link2.SetBodyColor("Red");
        link2.SetInteriorColor("Black");
        link2.SetWheelsColor("White");
        link2.WriteInterfaceName();
        link2.WriteBodyColor();
        link2.WriteInteriorColor();
        link2.WriteWheelsColor();
    }
}

public class Bicycle : Vehicle
{
    private static string class_name = "Bicycle";
    public const string type = "Terrestrial";

    // private string _condition;

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No Condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Bicycle.class_name);
    }
}

public class Airplane : Vehicle, IPassportData
{
    private static string class_name;
    public const string type = "Aerial";
    public readonly int maxFlightHeight;
    // private string _condition;
    private Airplane() { }
    public Airplane(string name, int weight, string country, int year, int maxFlightHeight_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        maxFlightHeight = maxFlightHeight_;
        Value = value;
    }
    public Airplane(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Airplane()
    {
        class_name = "Airplane";
    }

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }

    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Airplane(Name, Weight, Country, Year, maxFlightHeight, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Max flight height: " + maxFlightHeight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Airplane.class_name);
    }
}

public class Boat : Vehicle
{
    private static string class_name;
    public const string type = "Aquatic";
    public readonly int deadweight;
    // private string _condition;
    private Boat() { }
    public Boat(string name, int weight, string country, int year, int dead_weight, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        deadweight = dead_weight;
        Value = value;
    }
    public Boat(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Boat()
    {
        class_name = "Boat";
    }

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }
    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Deadweight: " + deadweight);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Boat.class_name);
    }
}

public class Motorbike : Vehicle, IPassportData
{
    private static string class_name;
    public const string type = "Terrestrial";
    public readonly int wheelsAmount;
    // private string _condition;
    private Motorbike() { }
    public Motorbike(string name, int weight, string country, int year, int wheelsAmount_, int value)
    {
        Name = name;
        Weight = weight;
        Country = country;
        Year = year;
        wheelsAmount = wheelsAmount_;
        Value = value;
    }
    public Motorbike(string name, int weight, string country)
    {
        Name = name;
        Weight = weight;
        Country = country;
    }
    static Motorbike()
    {
        class_name = "Motorbike";
    }

    public override void SetCondition(string condition)
    {
        _condition = condition;
    }
    public override string GetCondition()
    {
        if (_condition != null)
            return _condition;
        throw new Exception("No condition");
    }

    public static string GetClassName()
    {
        return class_name;
    }

    void IPassportData.WriteInterfaceName()
    {
        Console.WriteLine("Interface name: IPassportData");
    }

    void IPassportData.WriteCountry()
    {
        Console.WriteLine("Country:" + Country);
    }

    void IPassportData.WriteName()
    {
        Console.WriteLine("Name: " + Name);
    }

    void IPassportData.WriteWeight()
    {
        Console.WriteLine("Weight: " + Weight);
    }

    void IPassportData.WriteYear()
    {
        Console.WriteLine("Year: " + Year);
    }

    void IPassportData.WriteValue()
    {
        Console.WriteLine("Value: " + Value);
    }

    public override void ShowInfo()
    {
        Vehicle vehicle;
        vehicle = new Motorbike(Name, Weight, Country, Year, wheelsAmount, Value);
        var link1 = (IPassportData)vehicle;
        link1.WriteInterfaceName();
        link1.WriteName();
        link1.WriteWeight();
        link1.WriteYear();
        link1.WriteCountry();
        link1.WriteValue();
        Console.WriteLine("Name: " + Name);
        Console.WriteLine("Weight: " + Weight);
        Console.WriteLine("Country: " + Country);
        Console.WriteLine("Year: " + Year);
        Console.WriteLine("Value: " + Value);
        Console.WriteLine("Wheels amount: " + wheelsAmount);
        Console.WriteLine("Type: " + type);
        Console.WriteLine("Class name: " + Motorbike.class_name);
    }
}

public class Vertibird : Vehicle
{
    private static string class_name = "Vertibird";
    private static string type = "Aerial";

    private static Vertibird instance;
    // private string _condition;

    private Vertibird()
    {
        Console.WriteLine("In Vertibird private constructor");
    }

    public override void SetCondition(string condition) { }
    public override string GetCondition() { return "static"; }

    public static string GetClassName()
    {
        return class_name;
    }

    public static string GetClassType()
    {
        return type;
    }

    public static Vertibird getInstance()
    {
        if (instance == null)
            instance = new Vertibird();
        return instance;
    }

    public override void ShowInfo()
    {
        Console.WriteLine("Info: Vertibird");
    }
}

public class VehicleCondition
{
    private VehicleCondition() { }
    public static string GetGreatCondition()
    {
        return "Great";
    }
    public static string GetGoodCondition()
    {
        return "Good";
    }
    public static string GetNormalCondition()
    {
        return "Normal";
    }
    public static string GetWrackedCondition()
    {
        return "Wracked";
    }
}

public class MyException : Exception
{
    public MyException(string message)
        : base(message)
    {
        if (message == VehicleCondition.GetWrackedCondition())
        {
            Console.WriteLine("In MyException class, GetWrackedCondition()");
        }
    }
}

class Handler
{
    public void Message(string message, Vehicle vehicle)
    {
        if (message == "Cheap")
        {
            try
            {
                if (vehicle.GetCondition() == VehicleCondition.GetWrackedCondition())
                {
                    Console.WriteLine("Repairing...");
                    vehicle.SetCondition(VehicleCondition.GetGreatCondition());
                    Console.WriteLine("Repairing done!");
                }
                else 
                {
                    Console.WriteLine("Vehicle is in good condition. No repair needed");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
        else 
        {
            Console.WriteLine("The vehicle is too expensive. Can not reapir if needed");
        }
        Console.WriteLine(message);
    }
}

public class CarValue
{
    public delegate void MethodContainer(string message, Vehicle vehicle);
    public event MethodContainer Notify;

    public void getCarValue(Vehicle car)
    {
        if (car.Value < 10000)
        {
            Notify?.Invoke("Cheap", car);
        }
        else if (car.Value < 30000)
        {
            Notify?.Invoke("Comfort", car);
        }
        else if (car.Value < 100000)
        {
            Notify?.Invoke("Business", car);
        }
        else
        {
            Notify?.Invoke("Luxurious", car);
        }
    }
}

class Program
{
    static void Main()
    {
        Vehicle vehicle;

        vehicle = new Car("Fiat", 1490, "France", 2010, 4, 9999);
        vehicle.SetCondition(VehicleCondition.GetGoodCondition());
        Handler handler = new Handler();
        CarValue carValue = new CarValue();
        carValue.Notify += handler.Message;
        carValue.getCarValue(vehicle);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Bicycle();
        try 
        {
            Console.WriteLine(vehicle.GetCondition());
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Airplane("Boeing", 10000, "USA", 2004, 1500, 5000000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Boat("Boat", 10000, "China", 2005, 20000, 4000000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = new Motorbike("Ducatti", 330, "Italy", 2014, 2, 15000);
        vehicle.ShowInfo();
        Console.WriteLine("---------------------------------------------");
        vehicle = Vertibird.getInstance();
        vehicle.ShowInfo();
    }
}